MEE.Data.images = {
	'toolbar/arrow_down.png': { left: 0, top: 0, width: 5, height: 3 },
	'toolbar/arrow.png': { left: 5, top: 0, width: 4, height: 7 },
	'toolbar/home_spacer.png': { left: 9, top: 0, width: 16, height: 13 },
	'tbicons/accents.png': { left: 25, top: 0, width: 16, height: 16 },
	'tbicons/frac-menu.png': { left: 41, top: 0, width: 16, height: 16 },
	'toolbar/arrow_down-16.png': { left: 57, top: 0, width: 5, height: 16 },
	'tbicons/textrm.png': { left: 62, top: 0, width: 16, height: 16 },
	'tbicons/textit.png': { left: 78, top: 0, width: 16, height: 16 },
	'tbicons/textbf.png': { left: 94, top: 0, width: 16, height: 16 },
	'tbicons/sqrt-menu.png': { left: 110, top: 0, width: 16, height: 16 },
	'tbicons/row_insert.png': { left: 126, top: 0, width: 16, height: 16 },
	'tbicons/row_delete.png': { left: 142, top: 0, width: 16, height: 16 },
	'tbicons/row_append.png': { left: 158, top: 0, width: 16, height: 16 },
	'tbicons/mod.png': { left: 174, top: 0, width: 16, height: 16 },
	'tbicons/isotope.png': { left: 190, top: 0, width: 16, height: 16 },
	'toolbar/arrow-16.png': { left: 206, top: 0, width: 4, height: 16 },
	'tbicons/dots.png': { left: 210, top: 0, width: 16, height: 16 },
	'tbicons/blank_1x16.png': { left: 226, top: 0, width: 1, height: 16 },
	'tbicons/col_append.png': { left: 227, top: 0, width: 16, height: 16 },
	'tbicons/col_insert.png': { left: 243, top: 0, width: 16, height: 16 },
	'tbicons/col_delete.png': { left: 259, top: 0, width: 16, height: 16 },
	'toolbar/sizes/75x22-click.png': { left: 275, top: 0, width: 75, height: 22 },
	'toolbar/sizes/102x22-over.png': { left: 350, top: 0, width: 102, height: 22 },
	'toolbar/sizes/102x22-click.png': { left: 452, top: 0, width: 102, height: 22 },
	'toolbar/sizes/22x22-click.png': { left: 554, top: 0, width: 22, height: 22 },
	'toolbar/sizes/22x22-over.png': { left: 576, top: 0, width: 22, height: 22 },
	'toolbar/sizes/75x22-over.png': { left: 598, top: 0, width: 75, height: 22 },
	'toolbar/sizes/110x22-over.png': { left: 673, top: 0, width: 110, height: 22 },
	'toolbar/sizes/100x22-over.png': { left: 0, top: 22, width: 100, height: 22 },
	'toolbar/sizes/100x22-click.png': { left: 100, top: 22, width: 100, height: 22 },
	'toolbar/orb_hover.png': { left: 200, top: 22, width: 56, height: 22 },
	'toolbar/orb.png': { left: 256, top: 22, width: 56, height: 22 },
	'toolbar/sizes/110x22-click.png': { left: 312, top: 22, width: 110, height: 22 },
	'toolbar/sizes/100x24-over.png': { left: 422, top: 22, width: 100, height: 24 },
	'toolbar/sizes/100x24-click.png': { left: 522, top: 22, width: 100, height: 24 },
	'toolbar/sizes/64x24-over.png': { left: 622, top: 22, width: 64, height: 24 },
	'toolbar/sizes/64x24-click.png': { left: 686, top: 22, width: 64, height: 24 },
	'toolbar/sizes/130x24-click.png': { left: 0, top: 46, width: 130, height: 24 },
	'toolbar/sizes/96x24-over.png': { left: 130, top: 46, width: 96, height: 24 },
	'toolbar/sizes/53x24-over.png': { left: 226, top: 46, width: 53, height: 24 },
	'toolbar/sizes/53x24-click.png': { left: 279, top: 46, width: 53, height: 24 },
	'toolbar/sizes/50x24-click.png': { left: 332, top: 46, width: 50, height: 24 },
	'toolbar/sizes/50x24-over.png': { left: 382, top: 46, width: 50, height: 24 },
	'toolbar/sizes/96x24-click.png': { left: 432, top: 46, width: 96, height: 24 },
	'toolbar/sizes/130x24-over.png': { left: 528, top: 46, width: 130, height: 24 },
	'toolbar/sizes/32x32-over.png': { left: 658, top: 46, width: 32, height: 32 },
	'toolbar/sizes/32x32-click.png': { left: 690, top: 46, width: 32, height: 32 },
	'toolbar/home_redo.png': { left: 722, top: 46, width: 32, height: 32 },
	'toolbar/home_undo_g.png': { left: 754, top: 46, width: 32, height: 32 },
	'toolbar/home_undo.png': { left: 0, top: 78, width: 32, height: 32 },
	'toolbar/home_tick_blank.png': { left: 32, top: 78, width: 32, height: 32 },
	'toolbar/sizes/220x32-over.png': { left: 64, top: 78, width: 220, height: 32 },
	'toolbar/home_tick.png': { left: 284, top: 78, width: 32, height: 32 },
	'tbicons/basic.png': { left: 316, top: 78, width: 32, height: 32 },
	'toolbar/sizes/220x32-click.png': { left: 348, top: 78, width: 220, height: 32 },
	'tbicons/font.png': { left: 568, top: 78, width: 32, height: 32 },
	'toolbar/home_redo_g.png': { left: 600, top: 78, width: 32, height: 32 },
	'tbicons/greek.png': { left: 632, top: 78, width: 32, height: 32 },
	'tbicons/relations.png': { left: 664, top: 78, width: 32, height: 32 },
	'tbicons/operators.png': { left: 696, top: 78, width: 32, height: 32 },
	'tbicons/chem.png': { left: 728, top: 78, width: 32, height: 32 },
	'tbicons/brackets.png': { left: 760, top: 78, width: 32, height: 32 },
	'toolbar/home_clear.png': { left: 0, top: 110, width: 32, height: 32 },
	'tbicons/matrix-menu.png': { left: 32, top: 110, width: 32, height: 32 },
	'tbicons/sqrt-3.png': { left: 64, top: 110, width: 32, height: 32 },
	'tbicons/sqrt-4.png': { left: 96, top: 110, width: 32, height: 32 },
	'tbicons/bracket_rtype.png': { left: 128, top: 110, width: 32, height: 32 },
	'tbicons/sqrt-n.png': { left: 160, top: 110, width: 32, height: 32 },
	'tbicons/log.png': { left: 192, top: 110, width: 32, height: 32 },
	'tbicons/sqrt.png': { left: 224, top: 110, width: 32, height: 32 },
	'tbicons/subscript.png': { left: 256, top: 110, width: 32, height: 32 },
	'tbicons/superscript.png': { left: 288, top: 110, width: 32, height: 32 },
	'tbicons/symbols.png': { left: 320, top: 110, width: 32, height: 32 },
	'tbicons/text.png': { left: 352, top: 110, width: 32, height: 32 },
	'tbicons/bracket_rsize.png': { left: 384, top: 110, width: 32, height: 32 },
	'tbicons/frac.png': { left: 416, top: 110, width: 32, height: 32 },
	'tbicons/bracket_ltype.png': { left: 448, top: 110, width: 32, height: 32 },
	'tbicons/bracket_lsize.png': { left: 480, top: 110, width: 32, height: 32 },
	'tbicons/trig.png': { left: 512, top: 110, width: 32, height: 32 },
	'tbicons/boxed.png': { left: 544, top: 110, width: 32, height: 32 },
	'tbicons/bonds.png': { left: 576, top: 110, width: 32, height: 32 },
	'tbicons/arrows.png': { left: 608, top: 110, width: 32, height: 32 },
	'tbicons/reactions.png': { left: 640, top: 110, width: 32, height: 32 },
	'toolbar/sizes/220x35-click.png': { left: 0, top: 142, width: 220, height: 35 },
	'toolbar/sizes/220x35-over.png': { left: 220, top: 142, width: 220, height: 35 },
	'toolbar/sizes/120x36-over.png': { left: 440, top: 142, width: 120, height: 36 },
	'toolbar/sizes/160x36-over.png': { left: 560, top: 142, width: 160, height: 36 },
	'toolbar/sizes/160x36-click.png': { left: 0, top: 178, width: 160, height: 36 },
	'toolbar/sizes/120x36-click.png': { left: 160, top: 178, width: 120, height: 36 },
	'toolbar/sizes/100x36-over.png': { left: 280, top: 178, width: 100, height: 36 },
	'toolbar/sizes/100x36-click.png': { left: 380, top: 178, width: 100, height: 36 },
	'toolbar/home_wysiwyg.png': { left: 480, top: 178, width: 55, height: 55 },
	'toolbar/home_raw.png': { left: 535, top: 178, width: 55, height: 55 },
	'toolbar/sizes/220x58-click.png': { left: 0, top: 233, width: 220, height: 58 },
	'toolbar/sizes/220x58-over.png': { left: 220, top: 233, width: 220, height: 58 },
	'toolbar/sizes/64x64-click.png': { left: 440, top: 233, width: 64, height: 64 },
	'toolbar/sizes/64x64-over.png': { left: 504, top: 233, width: 64, height: 64 },
	'tbicons/idotsint.png': { left: 568, top: 233, width: 64, height: 64 },
	'toolbar/sizes/60x69-click.png': { left: 632, top: 233, width: 60, height: 69 },
	'toolbar/sizes/60x69-over.png': { left: 692, top: 233, width: 60, height: 69 },
	'toolbar/divider.png': { left: 752, top: 233, width: 3, height: 90 },
	'tbicons/matrix6.png': { left: 0, top: 323, width: 96, height: 96 },
	'tbicons/matrix5.png': { left: 96, top: 323, width: 96, height: 96 },
	'tbicons/matrix4.png': { left: 192, top: 323, width: 96, height: 96 },
	'tbicons/matrix3.png': { left: 288, top: 323, width: 96, height: 96 },
	'tbicons/matrix2.png': { left: 384, top: 323, width: 96, height: 96 },
	'tbicons/matrix1.png': { left: 480, top: 323, width: 96, height: 96 },
	'toolbar/sizes/96x96-click.png': { left: 576, top: 323, width: 96, height: 96 },
	'toolbar/sizes/96x96-over.png': { left: 672, top: 323, width: 96, height: 96 },
'zzz': 'zzz' };
