const LiveReloadPlugin = require('webpack-livereload-plugin');
const path = require('path');
const swag = require('@ephox/swag');

module.exports = (grunt) => {
  const packageData = grunt.file.readJSON('package.json');
  const BUILD_VERSION = packageData.version + '-' + (process.env.BUILD_NUMBER ? process.env.BUILD_NUMBER : '0');
  const libPluginPath = 'lib/main/ts/Main.js';
  const scratchPath = 'scratch/compiled';
  const scratchPluginPath = 'scratch/compiled/plugin.js';
  const scratchPluginMinPath = 'scratch/compiled/plugin.min.js';
  const tsDemoSourceFile = path.resolve('src/demo/ts/Demo.ts');
  const jsDemoDestFile = path.resolve('scratch/compiled/demo.js');

  grunt.initConfig({
    pkg: packageData,

    clean: {
      dirs: [ 'dist', 'scratch' ]
    },

    eslint: {
      options: {
        fix: grunt.option('fix')
      },
      plugin: [ 'src/**/*.ts' ]
    },

    shell: {
      command: 'tsc'
    },

    rollup: {
      options: {
        treeshake: true,
        format: 'iife',
        onwarn: swag.onwarn,
        plugins: [
          swag.nodeResolve({
            basedir: __dirname,
            prefixes: {}
          }),
          swag.remapImports()
        ]
      },
      plugin: {
        files: [
          {
            src: libPluginPath,
            dest: scratchPluginPath
          }
        ]
      }
    },

    uglify: {
      options: {
        mangle: false,
        sourceMap: true
      },
      plugin: {
        files: [
          {
            src: scratchPluginPath,
            dest: scratchPluginMinPath
          }
        ]
      }
    },

    copy: {
      css: {
        files: [
          { src: [ 'CHANGELOG.txt', 'LICENSE.txt' ], dest: 'dist/maths-equation-editor', expand: true }
        ]
      },
      I18n: {
        files: [
          { src: [ 'langs/*' ], dest: 'dist/maths-equation-editor', expand: true }
        ]
      },
      icons: {
        files: [
          { src: [ 'icons/*' ], dest: 'dist/maths-equation-editor', expand: true }
        ]
      },
      mee: {
        files: [
          { cwd: 'mee', src: [ '**/*' ], dest: 'dist/maths-equation-editor/mee', expand: true }
        ]
      },
      html: {
        files: [
          { cwd: 'src/main/html', src: [ '*' ], dest: 'dist/maths-equation-editor', expand: true }
        ]
      },
      js: {
        files: [
          { cwd: 'src/main/js', src: [ '*' ], dest: 'dist/maths-equation-editor', expand: true },
          { cwd: scratchPath, src: [ '*' ], dest: 'dist/maths-equation-editor', expand: true }
        ]
      },
    },

    webpack: {
      options: {
        mode: 'development',
        watch: true
      },
      dev: {
        entry: tsDemoSourceFile,
        devtool: 'source-map',

        resolve: {
          extensions: [ '.ts', '.js' ]
        },

        module: {
          rules: [
            {
              test: /\.js$/,
              use: [ 'source-map-loader' ],
              enforce: 'pre'
            },
            {
              test: /\.ts$/,
              use: [
                {
                  loader: 'ts-loader',
                  options: {
                    transpileOnly: true,
                    experimentalWatchApi: true
                  }
                }
              ]
            }
          ]
        },

        plugins: [ new LiveReloadPlugin() ],

        output: {
          filename: path.basename(jsDemoDestFile),
          path: path.dirname(jsDemoDestFile)
        }
      }
    }
  });

  require('load-grunt-tasks')(grunt);
  grunt.loadNpmTasks('@ephox/swag');

  grunt.registerTask('version', 'Creates a version file', () => {
    grunt.file.write('dist/maths-equation-editor/version.txt', BUILD_VERSION);
  });

  grunt.registerTask('default', [
    'clean',
    'eslint',
    'shell',
    'rollup',
    'uglify',
    'copy',
    'version'
  ]);
};
